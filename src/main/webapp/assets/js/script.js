$(document).ready(function(){ 
    $("#klant_table").tablesorter(); 
    $("#search").keyup(function(){
        _this = this;
        // Show only matching TR, hide rest of them
        $.each($("#klant_table tbody tr"), function() {
            if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
               $(this).hide();
            else
               $(this).show();                
        });
    }); 
    $('form#delete').submit(function(event){
     if(!confirm("Weet u zeker dat u deze klant wilt verwijderen?")){
        event.preventDefault();
      }
    });

    $('#myTabs a').click(function (e) {
      e.preventDefault()
      $(this).tab('show')
    });
    var now = new Date();

    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);

    var today = now.getFullYear()+"-"+(month)+"-"+(day) ;

    $('.datepicker').val(today);

}); 