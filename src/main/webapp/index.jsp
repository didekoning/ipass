<!doctype html>
<html lang="en">
<head>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<link rel="stylesheet" type="text/css" href="assets/base.css">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
   <link rel='shortcut icon' type='image/x-icon' href='images/favicon.ico' />
  <title>Ipass</title>
</head>
<body>
<div class="wrap-content">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-offset-4">
				<div class="card">
					<h2 class="text-center">Login</h2>
					<form action="LoginServlet.do" method="post">
						<div class="input-group">
						  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-user"></i></span>
						  <input type="text" name="username" class="form-control" placeholder="Gebruikersnaam" aria-describedby="basic-addon1">
						</div>
						<div class="paddingsm"></div>
						<div class="input-group">
						  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-lock"></i></span>
						  <input type="password" name="password" class="form-control" placeholder="Wachtwoord" aria-describedby="basic-addon1">
						</div>
						<div class="paddingsm"></div>
						 <button type="submit" class="btn btn-default">Submit</button>
					</form>
				</div>
				<div class="paddinglg"></div>
				<c:if test="${msgs != null}">
					<div class="alert alert-danger" role="alert">${msgs} </div>	  
				</c:if>
			</div>
		</div>
		
	</div>
</div>

<jsp:include page="/includes/footer.jsp" />

