<jsp:include page="../includes/header.jsp" />
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="wrap-content">
	<div class="container">
		<div class="paddinglg"></div>
			<div class="row">
				<div class="col-xs-12">
					<ul class="nav nav-tabs" role="tablist">
					    <li role="presentation" class="active"><a href="#data" aria-controls="data" role="tab" data-toggle="tab">Gegevens aanpassen</a></li>
					    <li role="presentation"><a href="#worth" aria-controls="worth" role="tab" data-toggle="tab">Klantwaarde</a></li>
					    <li role="presentation"><a href="#history" aria-controls="history" role="tab" data-toggle="tab">Klantgeschiedenis</a></li>
					     <li role="presentation"><a href="#dienst" aria-controls="dienst" role="tab" data-toggle="tab">Nieuwe dienst afgenomen</a></li>
				  	</ul>
				</div>
			</div>
			<div class="tab-content">
			    <div role="tabpanel" class="tab-pane active" id="data">
				    <div class="whitebg">
				    	<div class="paddingsm"></div>
				    		<c:if test="${msgs != null}">
								<div class="alert alert-danger" role="alert">${msgs} </div>	  
								<div class="paddingsm"></div>
							</c:if>
							<c:if test="${succes != null}">
								<div class="alert alert-success" role="alert">${succes} </div>	  
								<div class="paddingsm"></div>
							</c:if>
					   <form action="/admin/klant_info" method="post">
							<div class="row">
								<div class="col-sm-6">
								 <input type="text" name="klant_id" class="display_none" id="klant_id" value="${klantInfo.ID}" >
								  <div class="form-group">
								    <label for="voornaam">Voornaam</label>
								    <input type="text" name="voornaam" class="form-control" id="voornaam" value="${klantInfo.voornaam}" readonly>
								  </div>
								  <div class="form-group">
								    <label for="achternaam">Achternaam</label>
								    <input type="text" name="achternaam" class="form-control" id="achternaam" value="${klantInfo.achternaam}" readonly>
								  </div>
								   <div class="form-group">
								    <label for="gbdatum">Geboorte datum</label>
								    <input type="date" name="gbdatum" class="form-control" id="gbdatum" value="${klantInfo.gbdatum}" readonly>
								  </div>
					
								  <div class="paddingmd"></div>
								</div>
								
								
								<div class="col-sm-6">
								  <div class="form-group">
								    <label for="email">Email</label>
								    <input type="email" name="email" class="form-control" id="email" value="${klantInfo.email}" placeholder="Email">
								  </div>
								  <div class="form-group">
								    <label for="woonplaats">Woonplaats</label>
								    <input type="text" name="woonplaats" class="form-control" id="woonplaats" value="${klantInfo.woonplaats}" required>
								  </div>
								   <div class="form-group">
								    <label for="telefoon">Telefoon</label>
								    <input type="text" name="telefoon" class="form-control" id="telefoon" value="${klantInfo.telefoon}" required>
								  </div>
								  <div class="form-group">
								    <label for="personen">Personen in huishouden</label>
								    <input type="number" name="personen" class="form-control bfh-number" id="personen" value="${klantInfo.huishouden}" min="1" max="20" required>
								  </div>
								  <div class="form-group">
								    <label for="gbdatum">Actief:</label>
								    <select name="active" class="form-control" id="active">
								    <option value="1" <c:if test="${klantInfo.active == 1}">selected</c:if>>Actief</option>
								    <option value="0" <c:if test="${klantInfo.active == 0}">selected</c:if>>Inactief</option>
								    </select>
								  </div>
								  <div class="form-group">
								    <input type="submit" class="btn btn-primary text-right" value="Aanpassen">
								  </div>
								  <div class="paddingmd"></div>
								</div>

							</div>
						</form>
				    </div>
			    </div>
			    <div role="tabpanel" class="tab-pane" id="worth">
			    <div class="whitebg worth">
			    	<div class="paddingsm"></div>
			    	<div class="row">
						<div class="col-sm-3">
							<p>Totale opbrengst:</p>
							<p>${totaal}</p>
						</div>
						<div class="col-sm-3">
							<p>Aantal bezoeken:</p>
							<p>${bezoeken}</p>
						</div>
						<div class="col-sm-3">
							<p>Gem. opbrengst per bezoek:</p>
							<p>${gemopbrengst}</p>
						</div>
						<div class="col-sm-3">
							<p>Laatste bezoek:</p>
							<p><fmt:formatDate pattern="dd-MM-yyyy"  value="${laatstebezoek}" /></p>
						</div>
					</div>
					<div class="paddingsm"></div>
				</div>
			    </div>
			    <div role="tabpanel" class="tab-pane" id="history">
			    <div class="whitebg">
			    	<div class="row">
			    	<div class="col-sm-12">
			    	<h3>Orderhistory</h3>
			    	<table class="table table-hover">
			    		<thead>
					      <tr>
					        <th>Naam</th>
					        <th>Opbrengst</th>
					        <th>Datum</th>
					      </tr>
					    </thead>
						<tbody>
							<c:forEach var="dienst" items="${afgenomenDiensten}">
							<tr>
							<td>${dienst.naam}</td>
							<td>${dienst.opbrengst}</td>
							<td><fmt:formatDate pattern="dd-MM-yyyy"  value="${dienst.datum}" /></td>
							</tr>
							</c:forEach>
						</tbody>
						</table>
						  <div class="paddingmd"></div>
						</div>
					</div>
			    </div>
			    </div>
			   <div role="tabpanel" class="tab-pane" id="dienst">
			    <div class="whitebg">
			    	<div class="paddingsm"></div>
			    		<c:if test="${msgs_dienst != null}">
							<div class="alert alert-danger" role="alert">${msgs_dienst} </div>	  
							<div class="paddingsm"></div>
						</c:if>
						<c:if test="${succes_dienst != null}">
							<div class="alert alert-success" role="alert">${succes_dienst} </div>	  
							<div class="paddingsm"></div>
						</c:if>
				   <form action="/admin/nieuwe_dienst" method="post">
						<div class="row">
							<div class="col-sm-6">
							 <input type="text" name="klant_id" class="display_none" id="klant_id" value="${klantInfo.ID}" >
							  <div class="form-group">
							    <label for="voornaam">Naam</label>
							    <input type="text" name="naam" class="form-control" id="naam" required>
							  </div>
							  <div class="form-group">
							    <label for="achternaam">Datum</label>
							    <input type="date" name="datum" class="form-control datepicker" id="datum" required>
							  </div>
				
							  <div class="paddingmd"></div>
							</div>
							
							<div class="col-sm-6">
							  <div class="form-group">
							    <label for="achternaam">Opbrenst</label>
							    <input type="number" step="0.01" name="opbrengst" class="form-control" id="opbrengst" required>
							  </div>
							  <div class="form-group">
							  <br>
							    <input type="submit" class="btn btn-primary text-right" value="Toevoegen">
							  </div>
							  <div class="paddingmd"></div>
							</div>

						</div>
					</form>
			    </div>
		    </div>
		  	</div>

	</div>
	<div class="add_klant">
		<a href="/admin/new_klant.jsp"><i class="fa fa-plus"></i></a>
	</div>
</div>


<jsp:include page="../includes/footer.jsp" />

