<jsp:include page="../includes/header.jsp" />
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="ipass.domain.service.ServiceProvider" %>
<% request.setAttribute("service", ServiceProvider.getKlantenService()); %>
<div class="wrap-content">
	<div class="container">
		<div class="paddinglg"></div>
		<div class="whitebg">
		<div class="paddingsm"></div>
			<c:if test="${msgs != null}">
		<div class="alert alert-danger" role="alert">${msgs} </div>	  
		<div class="paddingsm"></div>
	</c:if>
	<c:if test="${succes != null}">
		<div class="alert alert-success" role="alert">${succes} </div>	  
		<div class="paddingsm"></div>
	</c:if>
		<div class="row">
			<div class="col-sm-6"><h1 class="title">Klantoverzicht</h1></div>
			<div class="col-sm-6"><input type="text" class="form-control" id="search" placeholder="Typ om te zoeken..." /></div>
		</div>
		
		<div class="paddingmd"></div>
		<table class="table table-striped table-bordered tablesorter" id="klant_table">
			<thead>
				<tr>
					<th>Voornaam</th>
					<th>Achternaam</th>
					<th>Geboortedatum</th>
					<th>Email</th>
					<th>Woonplaats</th>
					<th>Formaat huishouden</th>
					<th>Telefoon</th>
					<th>Actief</th>
					<th>Details</th>
					<th>Verwijderen</th>
				</tr>
			</thead>
			<tbody>
			<c:forEach var="klant" items="${service.getKlanten()}">
				<tr>
					<td>${klant.voornaam}</td>
					<td>${klant.achternaam}</td>
					<td>${klant.gbdatum}</td>
					<td>${klant.email}</td>
					<td>${klant.woonplaats}</td>
					<td>${klant.huishouden}</td>
					<td>${klant.telefoon}</td>
					<c:choose>
					<c:when test="${klant.active == 1}">
						<td>Actief</td>
					</c:when>
					<c:otherwise>
						<td>Inactief</td>
					</c:otherwise>
					</c:choose>
					<td><form action="/admin/klant_info" method="get"><button class="btn btn-primary" name="klant_id" id="delete_button" value="${klant.ID}" type="submit">Details</button></form></td>
					<td><form action="/admin/delete" method="post" id="delete"><button class="btn btn-primary" name="delete" id="delete_button" type="submit" value="${klant.ID}">Verwijderen</button></form></td> 
				</tr>
			</c:forEach>
			</tbody>
		</table>
		<div class="paddingsm"></div>
		</div>
	</div>
	<div class="add_klant">
		<form action="/admin/refresh" method="post"><button class="refresh" name="refresh" id="refresh"  type="submit"><i class="fa fa-refresh"></i></button></form>
	</div>
</div>


<jsp:include page="../includes/footer.jsp" />

