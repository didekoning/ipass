<jsp:include page="../includes/header.jsp" />
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="ipass.domain.service.ServiceProvider" %>
<% request.setAttribute("service", ServiceProvider.getKlantenService()); %>
<div class="wrap-content">
	<div class="container">
	<div class="paddinglg"></div>
	<c:if test="${msgs != null}">
		<div class="alert alert-danger" role="alert">${msgs} </div>	  
		<div class="paddinglg"></div>
	</c:if>
	<c:if test="${succes != null}">
		<div class="alert alert-success" role="alert">${succes} </div>	  
		<div class="paddinglg"></div>
	</c:if>
		<div class="whitebg">
			<div class="paddingmd"></div>
			<h1>Klant aanmaken</h1>
			<div class="paddingsm"></div>
			<form action="CreateUserServlet.do" method="post">
				<div class="row">
					<div class="col-sm-6">
					  <div class="form-group">
					    <label for="voornaam">Voornaam</label>
					    <input type="text" name="voornaam" class="form-control" id="voornaam" placeholder="Voornaam" required>
					  </div>
					  <div class="form-group">
					    <label for="achternaam">Achternaam</label>
					    <input type="text" name="achternaam" class="form-control" id="achternaam" placeholder="Achternaam" required>
					  </div>
					   <div class="form-group">
					    <label for="gbdatum">Geboorte datum</label>
					    <input type="date" name="gbdatum" class="form-control" id="gbdatum" placeholder="Geboorte datum" required>
					  </div>
					  <div class="paddingmd"></div>
					</div>
					
					
					<div class="col-sm-6">
					  <div class="form-group">
					    <label for="email">Email</label>
					    <input type="email" name="email" class="form-control" id="email" placeholder="Email" required>
					  </div>
					  <div class="form-group">
					    <label for="woonplaats">Woonplaats</label>
					    <input type="text" name="woonplaats" class="form-control" id="woonplaats" placeholder="Woonplaats" required>
					  </div>
					  <div class="form-group">
					    <label for="personen">Personen in huishouden</label>
					    <input type="number" name="personen" class="form-control bfh-number" id="personen" value="1" min="1" max="20" required>
					  </div>
					  <div class="form-group">
					    <label for="personen">Telefoonnummer</label>
					    <input type="tel" name="telefoon" class="form-control" id="telefoon" required>
					  </div>
					  <div class="form-group">
					    <input type="submit" class="btn btn-primary text-right" value="aanmaken">
					  </div>
					  <div class="paddingmd"></div>
					</div>

				</div>
			</form>
		</div>
	</div>
</div>


<jsp:include page="../includes/footer.jsp" />

