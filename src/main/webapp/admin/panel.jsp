<jsp:include page="../includes/header.jsp" />
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page import="ipass.domain.service.ServiceProvider" %>
<% request.setAttribute("service", ServiceProvider.getDienstenService()); %>
<% request.setAttribute("klanten", ServiceProvider.getKlantenService().getBesteKlanten()); %>
 <script type="text/javascript">

      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart', 'bar']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        
        
        data.addColumn('string', 'Datum');
        data.addColumn('number', 'Omzet');
        <c:forEach var="klant" items="${service.getThisWeekProfit()}">
        data.addRow(['${klant.simpelDatum}',${klant.opbrengst}]);
    	</c:forEach>
        // Set chart options
        var options = {'title':'Omzet per dag',
                       'width':500,
                       'height':300};
        
        var data2 = new google.visualization.DataTable();
        
        
        data2.addColumn('string', 'Naam');
        data2.addColumn('number', 'Opbrengst');
        <c:forEach begin="0" end="${fn:length(klanten)-1}" step="2" var="loop">
        data2.addRow(['${klanten.get(loop)}',${klanten.get(loop+1)}]);
    	</c:forEach>
        // Set chart options
        var options2 = {'title':'Meest waardevolle klanten',
                       'width':500,
                       'height':300,
                       is3D: true};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById('bar_chart_div'));
        chart.draw(data, options);
        var chart2 = new google.visualization.ColumnChart(document.getElementById('bar_chart_div2'));
        chart2.draw(data2, options2);
        
        
      }
     
</script>

<div class="wrap-content">
	<div class="container">
		<div class="paddinglg"></div>
		<div class="row">
			<div class="col-sm-6">
				<div id="bar_chart_div"></div>
			</div>
			<div class="col-sm-6">
				<div id="bar_chart_div2"></div>
			</div>
		</div>
		
	</div>
</div>

<jsp:include page="../includes/footer.jsp" />

