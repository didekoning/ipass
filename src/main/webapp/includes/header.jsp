<!doctype html>
<html lang="en">
<head>
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
	<link rel="stylesheet" type="text/css" href="../assets/base.css">
	<script type="text/javascript" src="../assets/js/jquery/jquery-1.11.2.min.js"></script>
	<script type="text/javascript" src="../assets/js/script.js"></script>
	<script type="text/javascript" src="../assets/js/jquery/jquery.tablesorter.min.js"></script>
	<script type="text/javascript" src="../assets/js/bootstrap/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <link rel='shortcut icon' type='image/x-icon' href='../images/favicon.ico' />
  <title>Klantsysteem</title>
</head>
<body>
<header>
<div class="container">
	<nav id="navbar" class="navbar-collapse collapse">
		<ul class="nav navbar-nav">
          <li><a href="/admin/panel.jsp">Home</a></li>
          <li><a href="/admin/klanten.jsp">Actieve Klanten</a></li>
        </ul>
        <ul class="nav navbar-nav pull-right">
          <li><a href="/admin/alle_klanten.jsp">Alle klanten</a></li>
          <li><form action="/LogoutServlet.do" method="post"><input type="submit" class="menu-button" value="Uitloggen"></form></li>
        </ul>
	</nav>
</div>
</header>