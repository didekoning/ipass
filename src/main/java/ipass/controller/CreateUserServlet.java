package ipass.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ipass.domain.service.KlantenService;
import ipass.domain.service.ServiceProvider;

public class CreateUserServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String voornaam = req.getParameter("voornaam");
		String achternaam = req.getParameter("achternaam");
		String gbdatum = req.getParameter("gbdatum");
		String email = req.getParameter("email");
		String woonplaats = req.getParameter("woonplaats");
		int personen = Integer.parseInt(req.getParameter("personen"));
		String telefoon = req.getParameter("telefoon");
		Date dt = null;
		Date now = new Date();
		Calendar cal = Calendar.getInstance();
		Date today = cal.getTime();
		cal.add(Calendar.YEAR, -120); 
		Date oldYear = cal.getTime();
		RequestDispatcher rd = null;
		try{
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
			dt = df.parse(gbdatum);
		}catch(ParseException sqle){
			req.setAttribute("msgs_dienst", "De datum is onjuist!");
			rd = req.getRequestDispatcher("/admin/klant_info.jsp");
			rd.forward(req, resp);
		}
		if(voornaam.equals(null) || voornaam == null){
			req.setAttribute("msgs", "Voornaam is niet correct of leeg!");
			rd = req.getRequestDispatcher("/admin/new_klant.jsp");
		}else if(achternaam.equals(null) || achternaam == null){
			req.setAttribute("msgs", "Achternaam is niet correct of leeg!");
			rd = req.getRequestDispatcher("/admin/new_klant.jsp");
		}else if(gbdatum.equals(null) || gbdatum == null){
			req.setAttribute("msgs", "Geboortedatum is niet correct of leeg!");
			rd = req.getRequestDispatcher("/admin/new_klant.jsp");
		}else if(woonplaats.equals(null) || woonplaats == null){
			req.setAttribute("msgs", "Woonplaats is niet correct of leeg!");
			rd = req.getRequestDispatcher("/admin/new_klant.jsp");
		}else if(personen == 0){
			req.setAttribute("msgs", "Het aantal personen is niet correct of leeg!");
			rd = req.getRequestDispatcher("/admin/new_klant.jsp");
		}else if(dt.after(now) || dt.before(oldYear)){
			req.setAttribute("msgs", "Deze datum is niet juist! Deze mag niet meer dan 120 jaar eerder of in de toekomst liggen.");
			rd = req.getRequestDispatcher("/admin/new_klant.jsp");
		}else{
			KlantenService service = ServiceProvider.getKlantenService();
			if(service.addKlant(voornaam, achternaam, gbdatum, woonplaats, personen, email, telefoon) == 1){
				req.setAttribute("succes", "De klant is toegevoegd!");
				rd = req.getRequestDispatcher("/admin/new_klant.jsp");
			}else if(service.addKlant(voornaam, achternaam, gbdatum, woonplaats, personen, email, telefoon) == 2){
				req.setAttribute("msgs", "Er bestaat al een klant met dit email adres!");
				rd = req.getRequestDispatcher("/admin/new_klant.jsp");
			}else{
				req.setAttribute("msgs", "Er is iets fout gegaan, probeer het later opnieuw!");
				rd = req.getRequestDispatcher("/admin/new_klant.jsp");
			}
		}

		rd.forward(req, resp);
	}
}
