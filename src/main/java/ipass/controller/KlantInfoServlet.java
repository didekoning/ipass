package ipass.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ipass.domain.service.KlantenService;
import ipass.domain.service.ServiceProvider;
import ipass.model.Dienst;
import ipass.model.Klant;

public class KlantInfoServlet extends HttpServlet{
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int ID = Integer.parseInt(req.getParameter("klant_id"));
		int opbrengst_totaal = 0;
		
		RequestDispatcher rd = null;
	
		KlantenService service = ServiceProvider.getKlantenService();
		Klant k = service.getKlant(ID);
		ArrayList<Dienst> afgenomenDiensten = k.getAfgenomenDiensten();
		
		for(Dienst d : afgenomenDiensten){
			opbrengst_totaal += d.getOpbrengst();
		}
		
		req.setAttribute("klantInfo", k);
		req.setAttribute("bezoeken", afgenomenDiensten.size());
		req.setAttribute("totaal", opbrengst_totaal);
		try{
		    req.setAttribute("gemopbrengst", opbrengst_totaal/afgenomenDiensten.size());
		}catch(Exception e){
			req.setAttribute("gemopbrengst", 0);
		}
		req.setAttribute("afgenomenDiensten", afgenomenDiensten);
		try{
			req.setAttribute("laatstebezoek", afgenomenDiensten.get(afgenomenDiensten.size()-1).getDatum());
		}catch(Exception e){
			req.setAttribute("laatstebezoek", new Date());
		}
		rd = req.getRequestDispatcher("/admin/klant_info.jsp");

		rd.forward(req, resp);
		
	}
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int ID = Integer.parseInt(req.getParameter("klant_id"));
		String voornaam = req.getParameter("voornaam");
		String achternaam = req.getParameter("achternaam");
		String gbdatum = req.getParameter("gbdatum");
		String email = req.getParameter("email");
		String woonplaats = req.getParameter("woonplaats");
		int personen = Integer.parseInt(req.getParameter("personen"));
		String telefoon = req.getParameter("telefoon");
		int active = Integer.parseInt(req.getParameter("active"));
		
		RequestDispatcher rd = null;

		
		if(voornaam.equals(null) || voornaam == null){
			req.setAttribute("msgs", "Voornaam is niet correct of leeg!");
			rd = req.getRequestDispatcher("/admin/new_klant.jsp");
		}else if(achternaam.equals(null) || achternaam == null){
			req.setAttribute("msgs", "Achternaam is niet correct of leeg!");
			rd = req.getRequestDispatcher("/admin/new_klant.jsp");
		}else if(gbdatum.equals(null) || gbdatum == null){
			req.setAttribute("msgs", "Geboortedatum is niet correct of leeg!");
			rd = req.getRequestDispatcher("/admin/new_klant.jsp");
		}else if(woonplaats.equals(null) || woonplaats == null){
			req.setAttribute("msgs", "Woonplaats is niet correct of leeg!");
			rd = req.getRequestDispatcher("/admin/new_klant.jsp");
		}else if(personen == 0){
			req.setAttribute("msgs", "Het aantal personen is niet correct of leeg!");
			rd = req.getRequestDispatcher("/admin/new_klant.jsp");
		}else{
			KlantenService service = ServiceProvider.getKlantenService();
			if(service.updateKlant(ID, voornaam, achternaam, gbdatum, woonplaats, personen, email, telefoon, active) == 1){
				req.setAttribute("succes", "De klantgegevens zijn aangepast!");
				rd = req.getRequestDispatcher("/admin/klanten.jsp");
			}else if(service.addKlant(voornaam, achternaam, gbdatum, woonplaats, personen, email, telefoon) == 2){
				req.setAttribute("msgs", "Er bestaat al een klant met dit email adres!");
				rd = req.getRequestDispatcher("/admin/klanten.jsp");
			}else{
				req.setAttribute("msgs", "Er is iets fout gegaan, probeer het later opnieuw!");
				rd = req.getRequestDispatcher("/admin/klanten.jsp");
			}
		}

		rd.forward(req, resp);
		
	}
}
