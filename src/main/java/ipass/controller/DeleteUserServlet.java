package ipass.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ipass.domain.service.KlantenService;
import ipass.domain.service.ServiceProvider;

public class DeleteUserServlet extends HttpServlet{
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int klantId = Integer.parseInt(req.getParameter("delete"));
		
		RequestDispatcher rd = null;
		
		KlantenService service = ServiceProvider.getKlantenService();
		if(service.deleteKlant(klantId)){
			req.setAttribute("succes", "De klant is verwijderd!");
			rd = req.getRequestDispatcher("/admin/klanten.jsp");
		}else{
			req.setAttribute("msgs", "Er is iets fout gegaan, probeer het later opnieuw!");
			rd = req.getRequestDispatcher("/admin/klanten.jsp");
		}

		rd.forward(req, resp);
	}
}
