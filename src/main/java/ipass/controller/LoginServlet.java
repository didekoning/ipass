package ipass.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ipass.domain.service.AccountService;
import ipass.domain.service.ServiceProvider;
import ipass.model.User;

public class LoginServlet extends HttpServlet{
	
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		AccountService service = ServiceProvider.getAccountService();
		String username = req.getParameter("username");
		String password = req.getParameter("password");
		
		RequestDispatcher rd = null;
		
		if(service.checkLogin(username, password)){
			User u = service.getUser(username);
			req.getSession().setAttribute("user", u);
			rd = req.getRequestDispatcher("/admin/panel.jsp");
		}else{
			req.setAttribute("msgs", "Deze user bestaat niet!");
			rd = req.getRequestDispatcher("index.jsp");
		}
		rd.forward(req, resp);
	}

}
