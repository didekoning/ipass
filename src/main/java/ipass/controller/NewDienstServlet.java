package ipass.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ipass.domain.service.DienstenService;
import ipass.domain.service.ServiceProvider;

public class NewDienstServlet extends HttpServlet{
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int ID = Integer.parseInt(req.getParameter("klant_id"));
		String naam = req.getParameter("naam");
		double opbrengst = Double.parseDouble(req.getParameter("opbrengst"));
		String datum_temp = req.getParameter("datum");
		System.out.println(datum_temp);
		RequestDispatcher rd = null;
		Date dt = null;
		Date now = new Date();
		try{
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
			dt = df.parse(datum_temp);
		}catch(ParseException sqle){
			req.setAttribute("msgs_dienst", "De datum is onjuist!");
			rd = req.getRequestDispatcher("/admin/klanten.jsp");
			rd.forward(req, resp);
		}
		if(naam.equals(null) || naam == null){
			req.setAttribute("msgs", "Naam mag niet leeg zijn!");
			rd = req.getRequestDispatcher("/admin/klanten.jsp");
		}else if(opbrengst < 0){
			req.setAttribute("msgs", "Opbrengst moet meer dan 0 zijn!");
			rd = req.getRequestDispatcher("/admin/klanten.jsp");
		}else if(dt.after(now)){
			req.setAttribute("msgs", "De datum mag niet in de toekomst liggen!");
			rd = req.getRequestDispatcher("/admin/klanten.jsp");
		}else{
			DienstenService service = ServiceProvider.getDienstenService();
			if(service.addDienst(ID, naam, opbrengst, dt)){
				req.setAttribute("succes", "De dienst is toegevoegd!");
				rd = req.getRequestDispatcher("/admin/klanten.jsp");
			}else{
				req.setAttribute("msgs", "Er is iets fout gegaan, probeer het later opnieuw!");
				rd = req.getRequestDispatcher("/admin/klanten.jsp");
			}
		}

		rd.forward(req, resp);
		
	}
}
