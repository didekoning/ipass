package ipass.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ipass.domain.service.AccountService;
import ipass.domain.service.ServiceProvider;
import ipass.model.User;

public class LogoutServlet  extends HttpServlet{
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		req.getSession().setAttribute("user", null);
		HttpServletResponse httpResponse = (HttpServletResponse) resp;
		httpResponse.sendRedirect("/index.jsp");

		
	}
}
