package ipass.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ipass.domain.service.KlantenService;
import ipass.domain.service.ServiceProvider;

public class RefreshUsersServlet extends HttpServlet{
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		RequestDispatcher rd = null;
		
		KlantenService service = ServiceProvider.getKlantenService();
		if(service.deactivateKlanten()){
			req.setAttribute("succes", "De inactieve klanten zijn gedeactiveerd!");
			rd = req.getRequestDispatcher("/admin/alle_klanten.jsp");
		}else{
			req.setAttribute("msgs", "Er is iets fout gegaan, probeer het later opnieuw!");
			rd = req.getRequestDispatcher("/admin/alle_klanten.jsp");
		}

		rd.forward(req, resp);
	}
}
