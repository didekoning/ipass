package ipass.dao.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import java.sql.Connection;

import ipass.model.User;

public class AccountDAO extends BaseDAO{
	
	private List<User> selectUsers(String query){
		List<User> results = new ArrayList<User>();
		try (Connection con = super.getConnection()){
			Statement stmt = con.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);
			while(dbResultSet.next()){
				int userID = dbResultSet.getInt("id");
				String username = dbResultSet.getString("username");
				String name = dbResultSet.getString("name");
				User u = new User(userID, username, name);
				results.add(u);
			}
		}catch (SQLException sqle){
			sqle.printStackTrace();
		}
		return results;
	}
	public Boolean checkLogin(String username, String password){
		if(selectUsers("SELECT id, username, name FROM users WHERE username='"+username+"' AND password='"+password+"'").size() > 0){
			return true;
		}
		return false;
	}
	public User getUser(String username){
		return selectUsers("SELECT id, username, name FROM users WHERE username='"+username+"'").get(0);
	}
}
