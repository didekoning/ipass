package ipass.dao.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import ipass.model.Dienst;

public class DienstenDAO extends BaseDAO{
	private ArrayList<Dienst> selectDiensten(String query){
		ArrayList<Dienst> results = new ArrayList<Dienst>();
		try (Connection con = super.getConnection()){
			Statement stmt = con.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);
			while(dbResultSet.next()){
				int ID = dbResultSet.getInt("klant_id");
				String dienst_naam = dbResultSet.getString("dienst_naam");
				Double opbrengst = dbResultSet.getDouble("opbrengst");
				String datum_temp = dbResultSet.getString("datum");
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
				Date datum = df.parse(datum_temp);
				Dienst k = new Dienst(ID, dienst_naam, opbrengst, datum);
				results.add(k);
			}
		}catch (SQLException | ParseException sqle){
			sqle.printStackTrace();
		}
		return results;
	}
	private ArrayList<Dienst> selectOpbrengst(String query){
		ArrayList<Dienst> results = new ArrayList<Dienst>();
		try (Connection con = super.getConnection()){
			Statement stmt = con.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);
			while(dbResultSet.next()){
				double opbrengst = dbResultSet.getDouble(1);
				String datum_temp = dbResultSet.getString(2);
				DateFormat df = new SimpleDateFormat("yyyy-mm-dd"); 
				Date datum = df.parse(datum_temp);
				Dienst d = new Dienst(1, "graph", opbrengst, datum);
				results.add(d);
			}
		}catch (SQLException | ParseException sqle){
			sqle.printStackTrace();
		}
		return results;
	}
	private boolean insertDienst(String query, int ID, String naam, double opbrengst, Date datum_temp){
		try (Connection con = super.getConnection()){
			PreparedStatement preparedStatement = con.prepareStatement(query);
			preparedStatement.setInt(1, ID);
			preparedStatement.setString(2, naam);
			preparedStatement.setDouble(3, opbrengst);
			java.sql.Date sqlDate = new java.sql.Date(datum_temp.getTime());
			preparedStatement.setDate(4, sqlDate);
			preparedStatement .executeUpdate();
		}catch (SQLException sqle){
			sqle.printStackTrace();
			return false;
		}
		return true;
	}
	private boolean removeDienst(String query, int ID){
		try (Connection con = super.getConnection()){
			PreparedStatement preparedStatement = con.prepareStatement(query);
			preparedStatement.setInt(1, ID);
			preparedStatement .executeUpdate();
		}catch (SQLException sqle){
			sqle.printStackTrace();
			return false;
		}
		return true;
	}
	public ArrayList<Dienst> getDienstenByKlantID(int kID){
		return selectDiensten("SELECT * FROM afgenomen_diensten WHERE klant_id='"+kID+"'");
	}
	public boolean addDienst(int ID, String naam, double opbrengst, Date datum){
		return insertDienst("INSERT INTO afgenomen_diensten (klant_id, dienst_naam, opbrengst, datum) values (?,?,?,?)", ID, naam, opbrengst, datum);
	}	
	public boolean removeDienst(int ID){
		return removeDienst("DELETE FROM afgenomen_diensten WHERE ID=?", ID);
	}
	public ArrayList<Dienst> weekOpbrengst(){
		return selectOpbrengst("SELECT sum(opbrengst), datum FROM afgenomen_diensten WHERE datum BETWEEN  DATE_SUB(NOW(), INTERVAL 7 DAY) AND NOW() GROUP BY datum;");
	}
	public ArrayList<Dienst> getDienstenBetween(){
		return selectDiensten("SELECT sum(opbrengst) FROM afgenomen_diensten WHERE datum BETWEEN  DATE_SUB(NOW(), INTERVAL 7 DAY) AND NOW() GROUP BY datum;");
	}
}
