package ipass.dao.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.util.ArrayList;

import ipass.model.Dienst;
import ipass.model.Klant;

public class KlantenDAO extends BaseDAO{
	private DienstenDAO dienstenDAO = new DienstenDAO();
	
	private ArrayList<Klant> selectKlanten(String query){
		ArrayList<Klant> results = new ArrayList<Klant>();
		try (Connection con = super.getConnection()){
			Statement stmt = con.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);
			while(dbResultSet.next()){
				int klantID = dbResultSet.getInt("ID");
				String voornaam = dbResultSet.getString("voornaam");
				String achternaam = dbResultSet.getString("achternaam");
				String gbdatum = dbResultSet.getString("gbdatum");
				String woonplaats = dbResultSet.getString("woonplaats");
				int per_huishouden = dbResultSet.getInt("per_huishouden");
				String email = dbResultSet.getString("email");
				String telefoon = dbResultSet.getString("telefoon");
				int active = dbResultSet.getInt("active");
				Klant k = new Klant(klantID, voornaam, achternaam, gbdatum, woonplaats, per_huishouden, email, telefoon, active);
				ArrayList<Dienst> diensten = dienstenDAO.getDienstenByKlantID(k.getID());
				for(Dienst d : diensten){
					k.addDienst(d);
				}
				
				results.add(k);
			}
		}catch (SQLException sqle){
			sqle.printStackTrace();
		}
		return results;
	}
	
	private int insertKlanten(String query, String voornaam, String achternaam, String gbdatum, String woonplaats, int personen, String email, String telefoon, int active){
		try (Connection con = super.getConnection()){
			PreparedStatement preparedStatement = con.prepareStatement(query);
			preparedStatement.setString(1, voornaam);
			preparedStatement.setString(2, achternaam);
			preparedStatement.setString(3, gbdatum);
			preparedStatement.setString(4, woonplaats);
			preparedStatement.setInt(5, personen);
			preparedStatement.setString(6, email);
			preparedStatement.setString(7, telefoon);
			preparedStatement.setInt(8, active);
			preparedStatement .executeUpdate();
		}catch(SQLIntegrityConstraintViolationException e){
			return 2;
		}catch (SQLException sqle){
			sqle.printStackTrace();
			return 0;
		}
		return 1;
	}
	private boolean deleteKlanten(String query, int klantId){
		try (Connection con = super.getConnection()){
			PreparedStatement preparedStatement = con.prepareStatement(query);
			preparedStatement.setInt(1, klantId);
			preparedStatement .executeUpdate();
		}catch (SQLException sqle){
			sqle.printStackTrace();
			return false;
		}
		return true;
	}
	
	private ArrayList<String> selectBesteKlanten(String query){
		ArrayList<String> results = new ArrayList<String>();
		try (Connection con = super.getConnection()){
			Statement stmt = con.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);
			while(dbResultSet.next()){
				results.add(dbResultSet.getString("naam"));
				results.add(String.valueOf(dbResultSet.getDouble("opbrengst")));
			}
		}catch (SQLException sqle){
			sqle.printStackTrace();
		}
		return results;
	}
	private boolean deactivateKlanten(String query){
		try (Connection con = super.getConnection()){
			PreparedStatement preparedStatement = con.prepareStatement(query);
			preparedStatement .executeUpdate();
		}catch (SQLException sqle){
			sqle.printStackTrace();
			return false;
		}
		return true;
	}
	
	public ArrayList<Klant> getKlanten(){
		return selectKlanten("SELECT * FROM klanten");
	}
	public ArrayList<Klant> getActieveKlanten(){
		return selectKlanten("SELECT * FROM klanten WHERE active=1");
	}
	public Klant getKlant(int klantId){
		return selectKlanten("SELECT * FROM klanten WHERE ID='"+klantId+"'").get(0);
	}
	public boolean deleteKlant(int klantId){
		return deleteKlanten("DELETE FROM klanten WHERE ID=?", klantId);
	}
	public boolean deactivateKlanten(){
		return deactivateKlanten("UPDATE klanten join afgenomen_diensten on klanten.ID=afgenomen_diensten.klant_id set klanten.active=0 where NOW() - INTERVAL 1 MONTH > datum;");
	}
	public ArrayList<String> getBesteKlanten(){
		return selectBesteKlanten("SELECT CONCAT(voornaam, ' ', achternaam) as naam, sum(opbrengst) as opbrengst FROM ipass.klanten inner join ipass.afgenomen_diensten on ipass.klanten.ID=ipass.afgenomen_diensten.klant_id group by voornaam order by opbrengst DESC limit 10;");
	}
	public int addKlant(String voornaam, String achternaam, String gbdatum, String woonplaats, int personen, String email, String telefoon){
		return insertKlanten("INSERT INTO klanten (voornaam, achternaam, gbdatum, woonplaats, per_huishouden, email, telefoon, active) values (?,?,?,?,?,?,?,?)", voornaam, achternaam, gbdatum, woonplaats, personen, email, telefoon, 1);
	}
	public int updateKlant(int ID, String voornaam, String achternaam, String gbdatum, String woonplaats, int personen, String email, String telefoon, int active){
		return insertKlanten("UPDATE klanten SET voornaam=?, achternaam=?, gbdatum=?, woonplaats=?, per_huishouden=?, email=?, telefoon=?, active=? WHERE ID='"+ID+"'", voornaam, achternaam, gbdatum, woonplaats, personen, email, telefoon, active);
	}
}
