package ipass.model;

import java.util.ArrayList;

public class Klant {
	private int ID;
	private String voornaam;
	private String achternaam;
	private String gbdatum;
	private String woonplaats;
	private int huishouden;
	private String email;
	private String telefoon;
	private int active;
	ArrayList<Dienst> afgenomenDiensten = new ArrayList<Dienst>();
	
	public Klant(int ID, String voornaam, String achternaam, String gbdatum, String woonplaats, int huishouden, String email, String telefoon, int active){
		this.ID = ID;
		this.voornaam = voornaam;
		this.achternaam = achternaam;
		this.gbdatum = gbdatum;
		this.woonplaats = woonplaats;
		this.huishouden = huishouden;
		this.email = email;
		this.telefoon = telefoon;
		this.active = active;
	}
	public int getID() {
		return ID;
	}

	public String getVoornaam() {
		return voornaam;
	}

	public String getAchternaam() {
		return achternaam;
	}

	public String getGbdatum() {
		return gbdatum;
	}

	public String getWoonplaats() {
		return woonplaats;
	}
	
	public String getEmail() {
		return email;
	}

	public int getHuishouden() {
		return huishouden;
	}
	
	public int getActive() {
		return active;
	}
	
	public String getTelefoon(){
		return telefoon;
	}
	
	public void addDienst(Dienst d){
		this.afgenomenDiensten.add(d);
	}
	
	public ArrayList<Dienst> getAfgenomenDiensten(){
		return this.afgenomenDiensten;
	}
	
}
