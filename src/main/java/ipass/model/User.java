package ipass.model;

public class User {
	private int ID;
	private String username;
	private String name;
	
	public User(int ID, String username, String name){
		this.ID = ID;
		this.username = username;
		this.name = name;
	}
	
	public String getUsername(){
		return this.username;
	}
	
	public String getName(){
		return this.name;
	}
}
