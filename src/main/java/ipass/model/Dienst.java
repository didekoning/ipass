package ipass.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Dienst {
	private int ID;
	private String naam;
	private double opbrengst;
	private Date datum;
	
	public Dienst(int ID, String naam, double opbrengst, Date datum){
		this.ID = ID;
		this.naam = naam;
		this.opbrengst = opbrengst;
		this.datum = datum;
	}
	public int getID(){
		return ID;
	}

	public String getNaam() {
		return naam;
	}

	public double getOpbrengst() {
		return opbrengst;
	}

	public Date getDatum() {
		return datum;
	}
	
	public String getSimpelDatum() {
		DateFormat df = new SimpleDateFormat("dd-mm-yyyy"); 
		String datum = df.format(this.datum);
		return datum;
	}
	
}
