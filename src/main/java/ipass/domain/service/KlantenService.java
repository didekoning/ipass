package ipass.domain.service;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import ipass.dao.persistence.KlantenDAO;
import ipass.model.Klant;

public class KlantenService {
	KlantenDAO dao = new KlantenDAO();
	
	public ArrayList<Klant> getKlanten(){
		return dao.getKlanten();
	}
	
	public ArrayList<Klant> getActieveKlanten(){
		return dao.getActieveKlanten();
	}
	public int addKlant(String voornaam, String achternaam, String gbdatum, String woonplaats, int personen, String email, String telefoon){
		return dao.addKlant(voornaam, achternaam, gbdatum, woonplaats, personen, email, telefoon);
	}
	
	public int updateKlant(int ID, String voornaam, String achternaam, String gbdatum, String woonplaats, int personen, String email, String telefoon, int active){
		return dao.updateKlant(ID, voornaam, achternaam, gbdatum, woonplaats, personen, email, telefoon, active);
	}
	
	public Boolean deleteKlant(int klantId){
		return dao.deleteKlant(klantId);
	}
	
	public Klant getKlant(int klantId){
		return dao.getKlant(klantId);
	}
	public ArrayList<String> getBesteKlanten(){
		return dao.getBesteKlanten();
	}
	public Boolean deactivateKlanten(){
		return dao.deactivateKlanten();
	}
}
