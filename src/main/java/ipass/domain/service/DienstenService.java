package ipass.domain.service;

import java.util.ArrayList;
import java.util.Date;

import ipass.dao.persistence.DienstenDAO;
import ipass.model.Dienst;

public class DienstenService {
	DienstenDAO dao = new DienstenDAO();
	
	public ArrayList<Dienst> getThisWeekProfit(){
		return dao.weekOpbrengst();
		
	}
	public boolean addDienst(int ID, String naam, double opbrengst, Date dt){
		return dao.addDienst(ID, naam, opbrengst, dt);
	}
	public boolean deleteDienst(int ID){
		return dao.removeDienst(ID);
	}
	
	
}
