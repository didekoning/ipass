package ipass.domain.service;

public class ServiceProvider {
	private static AccountService accountService = new AccountService();
	private static KlantenService klantService = new KlantenService();
	private static DienstenService dienstenService = new DienstenService();
	
	public static AccountService getAccountService(){
		return accountService;
	}
	
	public static KlantenService getKlantenService(){
		return klantService;
	}
	
	public static DienstenService getDienstenService(){
		return dienstenService;
	}

}
