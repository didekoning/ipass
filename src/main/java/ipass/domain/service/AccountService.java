package ipass.domain.service;

import ipass.dao.persistence.AccountDAO;
import ipass.model.User;

public class AccountService {
	AccountDAO dao = new AccountDAO();
	
	public Boolean checkLogin(String username, String password){
		return dao.checkLogin(username, password);
	}
	
	public User getUser(String username){
		return dao.getUser(username);
	}
	
}
